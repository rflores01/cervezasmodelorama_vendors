const token = 'WcKHBBk001PLvWsCeHfTlEaQwhO98JplGmcvkQaeRCGY9TrozZOOE4AslNKDvuyBi9a3VOWrQUF7HVtyg5BOG6cV9LvXNRs0SjYyrhexINmAIUVA0c8BUx6MUWibE39jHUGmPE0VoZbr7oxMPyW7r50aApnXR7ZuXMEh7DLI7Y4srlbEPpCs99k87ocENCL246RfyaFz1622941446';
var times = 0;
var delivery = [];

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  });

window.onload = () => {
    var eventSrc = new EventSource('http://localhost/cervezasmodelorama-event.php');
        eventSrc.onmessage = (e) => {
            times++;
            var data = JSON.parse(e.data);
            console.log(data,'<---');

            Toast.fire({
                icon: 'success',
                title: data.name.trim(),
                text: 'order create!'
              });

            var n = new Notification('New order entrance',{
                body: 'The order wating for you',
                icon: 'https://arvolution.com/wp-content/uploads/2019/03/grupo_modelo_logo-e1553570073779.png'
            });

            $('#label-alert').text(times + ' new order entrance');
            $('#label-alert').transition('zoom');

            clearFile();
        };

    /** GET DELIVERY STATUS */
    fetch('http://localhost/cervezasmodelorama/public/api/getDelivery',{
        method: 'GET',
        headers: {
            'api-token': token
        }
    }).then(rs => rs.json()).then(rs => {
        delivery = rs.data;

        getOrders();
    });

    if(Notification.permission !== 'denied')
    {
        Notification.requestPermission(permission => {
            //
        });
    }

    $('#label-alert').on('click', function() {
        getOrders();
        times = 0;
        $(this).transition('zoom');
    });
};

function getOrders() {
    var viewproduct = document.querySelector('#view-product');
    fetch('http://localhost/cervezasmodelorama/public/api/orders/'+1,{
        method: 'GET',
        headers: {
            'api-token': token
        }
    }).then(rs => rs.json()).then(rs => {

        $('#dimmer-loader').removeClass('active');
        var tbody = document.querySelector('#tbl-orders tbody'); 
            tbody.innerHTML = '';

        rs.data.map(el => {
            var tr = hex.tr('',[],tbody);
            
            hex.td(el.name,[],tr);
            hex.td(el.price,[],tr);
            hex.td(el.amount,[],tr);
            hex.td(el.total,[],tr);
            var td = hex.td('',[],tr);
                td.style.textAlign = 'center';
            var select = hex.select('',('ui dropdown').split(' '),td);
                delivery.map((d,n) => {
                    var option = hex.option(d.description,[],select);
                        option.value = d.id;

                    if(el.delivery == d.description)
                    {
                        option.selected = 'selected';
                    }

                    if((n+1) == delivery.length)
                    {
                        $(select).dropdown();
                    }
                });

                select.onchange = () => {
                    fetch('http://localhost/cervezasmodelorama/public/api/affectedOrder/'+el.id+'/'+select.value,{
                        method: 'GET',
                        headers: {
                            'api-token': token
                        }
                    }).then(rs => rs.json()).then(rs => {
                        if(rs.action != false)
                        {
                            Toast.fire({
                                icon: 'success',
                                title: el.name,
                                text: 'order update!'
                              });
                        }
                    });
                }
        });
    });
}

function clearFile() {
    fetch('http://localhost/cervezasmodelorama/public/api/clearFile',{
        method: 'GET',
        headers: {
            'api-token': token
        },
    }).then(rs => rs.json()).then(rs => {
        if(rs.action != false)
        {
            console.log('file clear!','<<<<');
        }
    });
}