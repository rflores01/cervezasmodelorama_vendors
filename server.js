const express = require('express');
const app = express();
const path = require('path');

app.use('/js',express.static(path.join(__dirname,'public','src','js')));
app.use('/commons',express.static(path.join(__dirname,'public','src','commons')));

app.get('/',(req,res) => {
    res.sendFile('index.html',{
        root: path.join(__dirname,'public','src')
    });
});

const PORT = process.env.PORT || 8088;
app.listen(PORT, () => {
    console.log('listen in port:', PORT);
});